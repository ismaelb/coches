class Coche:
    def __init__(self, color, marca, modelo, matricula, velocidad):
        self.color = color
        self.marca = marca
        self.modelo = modelo
        self.matricula = matricula
        self.velocidad = velocidad

    def acelerar(self, aumemtar_velocidad):
        self.velocidad = self.velocidad + aumemtar_velocidad
        return self.velocidad

    def frenar(self, disminuir_velocidad):
        self.velocidad = self.velocidad - disminuir_velocidad
        return self.velocidad
    
    def __str__(self):
        return f"Tu coche es de color {self.color}, de la marca {self.marca}, del modelo {self.modelo}, su matrícula es: {self.matricula} y va a una velocidad de: {self.velocidad} km/h"
    
coche1 = Coche("negro", "Lamborghini", "Urus", "IS5638", 50)
print(coche1)