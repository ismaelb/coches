import coches
import unittest

class MultiplyTestCase(unittest.TestCase):
    def test_acelerar(self):
        coche1 = coches.Coche("negro", "Lamborghini", "Urus", "IS5638", 50)
        coche1.acelerar(20)
        self.assertEqual(coche1.velocidad, 70)

    def test_frenar(self):
        coche1 = coches.Coche("negro", "Lamborghini", "Urus", "IS5638", 50)
        coche1.frenar(20)
        self.assertEqual(coche1.velocidad, 30)

if __name__=="__main__":
    unittest.main()